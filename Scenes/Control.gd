extends Control

var CurrentMovieName = []

enum {
	CORRECT,
	WRONG,
}


func _on_Submit_button_down():
	var movie_name = $HSplitContainer/MovieName.get_text()
	if movie_name.to_lower() in CurrentMovieName:
		Dialog.show_message(self, Dialog.DlgCorrectAnswer, "on_ok", [CORRECT])
	else:
		Dialog.show_message(self, Dialog.DlgWrongAnswer)


func on_ok(status):
	if status == CORRECT:
		$HSplitContainer/MovieName.text = ""
		Signals.emit_signal("NextLevel")

