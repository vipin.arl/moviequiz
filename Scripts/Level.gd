extends Node2D

const LEVEL_DATA_FILE = 'level.data'

var LevelCategory = [
	"dumbcharades/"
]

var LevelData = null
var TotalLevel = -1

var BaseURL = "https://gitlab.com/vipin.arl/moviequiz/-/raw/master/.levels/"
var CurrentLevel = 0
var CurrentType = 0

var HttpRequest = [
	HTTPRequest.new(),
	HTTPRequest.new(),
	HTTPRequest.new(),
	HTTPRequest.new(),
]


func _ready():
	for i in range(0,4):
		add_child(HttpRequest[i])
		HttpRequest[i].connect("request_completed", self, "http_image_response", [i+1])
	
	Signals.connect("NextLevel", self, "next_level")
	get_level_details()
	#Dialog.show_message(self, Dialog.CorrectAnswer)
	#Dialog.show_message(self, Dialog.DlgImageDownloadError)


func send_request(img):	
	var image_name = str(CurrentLevel) + "/" + str(img+1) + ".png"
	var image_url = BaseURL + LevelCategory[CurrentType] + image_name
	var error = HttpRequest[img].request(image_url)
	if error != OK:
		Dialog.show_message(self, Dialog.DlgInternetError)
		return


func http_image_response(result, response_code, _headers, body, img):
	if result != 0 || response_code != 200:
		Dialog.show_message(self, Dialog.DlgDownloadError)
		return
	
	var image = Image.new()
	var error = image.load_png_from_buffer(body)
	if error != OK:
		Dialog.show_message(self, Dialog.DlgWrongImage)
		return

	var texture = ImageTexture.new()
	texture.create_from_image(image)

	match img:
		1:
			$Image1.texture = texture
		2:
			$Image2.texture = texture
		3:
			$Image3.texture = texture
		4:
			$Image4.texture = texture


func next_level():
	if CurrentLevel == TotalLevel:
		Dialog.show_message(self, Dialog.DlgLevelOver)
		return
	
	$Control.CurrentMovieName = LevelData[CurrentLevel]["name"]
	CurrentLevel += 1
	for i in range (0, 4):
		send_request(i)


func get_level_details():
	var level_req = HTTPRequest.new()
	add_child(level_req)
	level_req.connect("request_completed", self, "http_level_response", [level_req])
	
	var level_url = BaseURL + LevelCategory[CurrentType] + LEVEL_DATA_FILE
	print(level_url)
	var error = level_req.request(level_url)
	if error != OK:
		Dialog.show_message(self, Dialog.DlgInternetError)
		return 0


func http_level_response(result, response_code, _headers, body, http_node):
	http_node.queue_free()
	if result != 0 || response_code != 200:
		Dialog.show_message(self, Dialog.DlgDownloadError)
		return
	
	var response = parse_json(body.get_string_from_utf8())
	if response.size() > 0:
		LevelData = response
		TotalLevel = LevelData.size()
		next_level()
