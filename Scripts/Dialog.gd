extends Node

enum {
	Title,
	Message,
}

enum {
	DlgDownloadError,
	DlgInternetError,
	DlgWrongImage,
	DlgCorrectAnswer,
	DlgWrongAnswer,
	DlgLevelOver,
}

var DialogMessages = [
	{
		Title: "Unlucky Timings",
		Message: "Error at fetch image because of a server upgrade.\n\nPlease try after a while!"
	},
	{
		Title: "Error",
		Message: "Couldn't send requests!\n\nPlease check your internet connection."
	},
	{
		Title: "Unlucky Timings",
		Message: "Couldn't load image, please try again later\n\nIf this contines, please contact the developer"
	},
	{
		Title: "Good one!",
		Message: "correct Answer"
	},
	{
		Title: "Try Again",
		Message: "Wrong answer!"
	},
	{
		Title: "You beat me buddy!",
		Message: "I really don't want this to happen, but you beat me!\n\nPlease comabck, there will be more fun loaded by then"
	}
]


func show_message(parent, msg, handler=null, args = []):
	var dialog = AcceptDialog.new()
	dialog.window_title = DialogMessages[msg][Title]
	dialog.dialog_text = DialogMessages[msg][Message]
	
	var okbtn = dialog.get_ok()
	okbtn.connect("pressed", self, "dlg_ok", [dialog])
	if handler != null:
		okbtn.connect("pressed", parent, handler, args)
	
	parent.add_child(dialog)
	dialog.popup_centered(Vector2(250, 75))


func dlg_ok(dlg_inst):
	dlg_inst.queue_free()
